class student:
    school="xyz"
    def __init__(self, m1, m2, m3):
        self.m1 = m1
        self.m2 = m2
        self.m3 = m3
        print("object assigned")
    def average(self):
        return (self.m1+self.m2+self.m3)/3
   # @classmethod
    def classmethod(self):
        self.school = "klm"
        return (self.school)
    @staticmethod
    def info():
        print("static method called")



s1=student(5, 10, 15)
s1.school = "abc"
s2=student(3,4,5)
student.school="def"
print("the school is", student.classmethod())

print(s2.school)
print( s1.average(), s1.school)
print("the class method is ", student.classmethod())
s1.info()
print(s1.school)
print(s2.school)