def division(a, b):
    print(a/b)
def smart_division(div):
    def inner(b, c):
        if b<c:
            b,c = c,b
        return div(b,c)
    return inner
div1 = smart_division(division)
div1(2,4)