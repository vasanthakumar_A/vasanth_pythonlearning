from abc import  ABC,abstractmethod
class aclass(ABC):
    @abstractmethod
    def display(self):
        None
    @abstractmethod
    def show(self):
        None
class bclass(aclass):
    def display(self):
        print("abstract display method called")
    def show(self):
        print("show method is called")


obj = bclass()
obj.display()