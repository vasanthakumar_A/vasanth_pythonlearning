my_dict = {"fruit":"apple", "number":"1", "year": "1997" }
print(my_dict["number"])
#change
my_dict["year"] = 1984
print(my_dict["year"])
#remove
my_dict.pop("fruit")
print(my_dict)
#add
my_dict["color"]="red"
#looping
for i in my_dict:
    print("the dictionary items are", i)