import unittest
import Calculation

class Testcases(unittest.TestCase):
    def test_addition(self):
        self.assertEqual(Calculation.add(10,15), 25)
        self.assertEqual(Calculation.add(-1, 1), 0)
        self.assertEqual(Calculation.add(-1, -1), -2)
        self.assertEqual(Calculation.add(1, -1), 0)

    def test_subraction(self):
        self.assertEqual(Calculation.subract(15, 10), 5)
        self.assertEqual(Calculation.subract(-1, 1), -2)
        self.assertEqual(Calculation.subract(-1, -1), 0)
        self.assertEqual(Calculation.subract(1, -1), 2)

    def test_multiply(self):
        self.assertEqual(Calculation.multiply(10, 15), 150)
        self.assertEqual(Calculation.multiply(-1, 1), -1)
        self.assertEqual(Calculation.multiply(-1, -1), 1)


    def test_division(self):
        self.assertEqual(Calculation.divide(10, 5), 2)
        self.assertEqual(Calculation.divide(-1, 1), -1)
        self.assertEqual(Calculation.divide(-1, -1), 1)
        with self.assertRaises(ValueError):
            Calculation.divide(5, 0)


if __name__ == '__main__':
    unittest.main()

