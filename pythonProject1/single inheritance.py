class parent:
    a=5
    def parentmethod(self):
        print("parentclass is called", self.a)

class child1(parent):
    b=4
    def childmethod(self):
        print("the child class called", self.b)


c = child1()
c.parentmethod()