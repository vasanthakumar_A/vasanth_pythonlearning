class a:
    def aclassmethod(self):
        print("a class is called")
class b(a):
    def bclassmethod(self):
        print("class b is called")
class c(b):
    def cclassmethod(self):
        print("class c method is called")
cobj = c()
cobj.aclassmethod()
cobj.bclassmethod()
cobj.cclassmethod()