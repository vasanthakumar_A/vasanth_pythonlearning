class mother:
    def motherclassmethod(self):
        print("mother class method is called")
class father:
    def fatherclassmethod(self):
        print("father class method is called")
class child(mother, father):
    def childclassmethod(self):
        print("child class method is called")
objc = child()
objc.motherclassmethod()
objc.fatherclassmethod()
objc.childclassmethod()
objf = father()
objf.fatherclassmethod()
objm =mother()
objm.motherclassmethod()