from selenium import webdriver
driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")
driver.get("http://newtours.demoaut.com/")
driver.implicitly_wait(10)  #if ny of the elements in the process takes long time (ex: if the username is not found then it will take some time to search,
# so it will make the process to wait for maximum of ten seconds till that time it will continue another process)

assert "Welcome: Mercury Tours" in driver.title  # it will check the given name and the driver title name are same returns true if both are equal else returns false

driver.find_element_by_name("username").send_keys("mercury")
driver.find_element_by_name("password").send_keys("mercury")

driver.find_element_by_name("login").click()