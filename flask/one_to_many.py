from flask import  Flask, request, jsonify
from  flask_sqlalchemy import SQLAlchemy
import uuid
import os
from sqlalchemy import types
from sqlalchemy.dialects.mysql.base import MSBinary
from sqlalchemy.schema import Column
import uuid

app = Flask(__name__)
class UUID(types.TypeDecorator):
    impl = MSBinary
    def __init__(self):
        self.impl.length = 16
        types.TypeDecorator.__init__(self,length=self.impl.length)

    def process_bind_param(self,value,dialect=None):
        if value and isinstance(value,uuid.UUID):
            return value.bytes
        elif value and not isinstance(value,uuid.UUID):
            pass
        else:
            return None

    def process_result_value(self,value,dialect=None):
        if value:
            return uuid.UUID(bytes=value)
        else:
            return None

    def is_mutable(self):
        return False
#db_password = os.environ['DB_PASS']
# print(db_password)

uid = uuid.uuid4()
print(uid)
uei = uuid.uuid4()
print(uei)





app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:root''@localhost/crud'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
def generateuid():
    return uuid.uuid4()

class base2(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20))
    age = db.Column(db.String(20))
    uid = db.Column(db.String(36), default=generateuid())
    pets = db.relationship('derived2', backref='owner')
class derived2(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20))
    pet_id = db.Column(db.Integer, db.ForeignKey('base2.id'))

if __name__ == "__main__":
    app.run(debug=True)