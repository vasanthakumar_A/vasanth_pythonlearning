from flask import  Flask, request, jsonify
from  flask_sqlalchemy import SQLAlchemy




app = Flask(__name__)


app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:root''@localhost/crud'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

class Parent(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(100))
    child = db.relationship('Child', backref='parent', uselist=False)




class Child(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(100))
    parent_id = db.Column(db.Integer, db.ForeignKey('parent.id'))

if __name__ == "__main__":
    app.run(debug=True)



