import json

import requests

try:
    from ormcrud import app

    import unittest
    from unittest.mock import patch

except Exception as e:
    print("some modules are missing")


class flasktest(unittest.TestCase):
    def test_index(self):
        tester = app.test_client(self)
        response = tester.get("/")
        statuscode = response.status_code
        self.assertEqual(statuscode, 200)
    @unittest.skip("skip add")
    def test_add_details(self):
        tester = app.test_client(self)
        info = {
        "author": "890",
        "description": "this is a 890 description",
        "title": "this is a 890 title"
        }
        response = tester.post('/post', json=info)
        self.assertEqual(response.status_code, 200)
    @unittest.skip("skip updated")
    def test_update_details(self):
        tester = app.test_client(self)
        info = {
            "author": "890",
            "description": "this is a 890 description",
            "title": "this is a 890 title"
        }
        response = tester.put('/update/8/', json=info)
        self.assertEqual(response.status_code, 200)
    @unittest.skip("skip delete")
    def test_delete_details(self):
        tester = app.test_client(self)
        response = tester.delete('/delete/11/')
        self.assertEqual(response.status_code, 200)

if __name__ == "__main__":
    unittest.main()