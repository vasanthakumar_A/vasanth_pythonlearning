import mysql.connector
from tabulate import tabulate
mydb = mysql.connector.connect(host="127.0.0.1", user="root", passwd="root",  auth_plugin='mysql_native_password', database = "employee")


def insert(name, age, city):

    mycursor = mydb.cursor()
    sql = "insert into details(NAME, AGE, city) values(%s, %s, %s)"
    user = (name, age, city)
    mycursor.execute(sql, user)
    mydb.commit()
    print("Data inserted")
def update(name, age, city, id):
    mycursor = mydb.cursor()
    sql = "update details set Name=%s, AGE = %s, city=%s where id=%s"
    user = (name, age, city, id)
    mycursor.execute(sql, user)
    mydb.commit()
    print("Data updated")
def select():
    mycursor = mydb.cursor()
    sql = "SELECT ID, NAME , AGE, city from details"
    mycursor.execute(sql)
   # res = mycursor.fetchone()  #fetches top data in the database
    res = mycursor.fetchall()
    print(tabulate(res, headers=["ID", "NAME", "AGE", "CITY"]))
def delete(id):
    mycursor = mydb.cursor()
    sql = "delete from details where id=%s"
    user = (id,)
    mycursor.execute(sql, user)
    mydb.commit()
    print("Data deleted")

while True:
    print("1. insert the data")
    print("2.  select the data")
    print("3. update the data")
    print("4. delete the data")
    print("5. exit")
    choice=int(input("Enter your choice: "))
    if choice == 1:
        n = (input("Enter the name: "))
        a = (input("Enter the age: "))
        c = (input("Enter the city: "))
        insert(n, a, c)
    elif choice ==2:
        select()
    elif choice == 3:
        n = (input("Enter the name: "))
        a = (input("Enter the age: "))
        c = (input("Enter the city: "))
        d = (input("Enter the id: "))
        update(n, a, c, d)
    elif choice == 4:
        a = input("Enter id to delete: ")
        delete(a)
    elif choice == 5:
        quit()
    else:
        print("invalid choice")


