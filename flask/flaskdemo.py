from flask import Flask, render_template, request, redirect, url_for
import helloworld
app = Flask(__name__)           #to get the name of the flask
@app.route('/')  #condition for the page should open if there is no words after .com
@app.route('/home')
def web():
    return render_template('register.html')
@app.route("/confirm", methods=['POST', 'GET'])
def register():
    if request.method=='POST':
        n = request.form.get('name')
        a = request.form.get('age')
        c = request.form.get('city')
        return render_template('confirm.html', name=n,age=a,city=c)
@app.route('/pass/<int:score>')
def success(score):
    return "the student passed"+str(score)
@app.route('/fail/<int:score>')
def failure(score):
    return "the student failed"+str(score)
@app.route('/result/<int:marks>')
def results(marks):
    result = ""
    if marks < 50:
        result = 'failure'

    else:
        result = 'success'
    return redirect(url_for(result,score=marks))


   #to avoid the process of reloading
