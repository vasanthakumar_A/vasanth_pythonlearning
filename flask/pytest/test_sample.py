from selenium import webdriver
from selenium.webdriver import ActionChains
def test_setup():
    global driver
    driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")
    driver.get("https://opensource-demo.orangehrmlive.com")

def test_login():
    driver.find_element_by_xpath("//*[@id='txtUsername']").send_keys("Admin")
    driver.find_element_by_xpath("//*[@id='txtPassword']").send_keys("admin123")
    driver.find_element_by_xpath("//*[@id='btnLogin']").click()

def test_teardown():
    driver.close()
    driver.quit()