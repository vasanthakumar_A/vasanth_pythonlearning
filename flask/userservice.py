from flask import  Flask, request, jsonify
from  flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
import uuid
import bcrypt
import os
import re
app = Flask(__name__)
# DB_PASSWORD = os.environ['DB_PASSWORD']
# print(DB_PASSWORD)
#print("the password", DB_PASSWORD)

app.config['SQLALCHEMY_DATABASE_URI'] =  'mysql://root:root@localhost/crud'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
ma = Marshmallow(app)
def truncateuid():
    data = str(uuid.uuid4().hex)
    info = (data[:16])
    return info
def emailvalidation(mail):
    regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
    if(re.match(regex, mail)):
        return mail
    else:
        return None
def isvalid(num):
    Pattern = re.compile("^[0-9]{10}$")
    return Pattern.match(num)

def generateuid():
    return uuid.uuid4()
truncateuid()
class Userservicedata(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    uid = db.Column(db.String(36), default=generateuid(), nullable=False)
    first_name = db.Column(db.String(128), nullable=False)
    last_name = db.Column(db.String(128), nullable=False)
    email = db.Column(db.String(128), nullable=False)
    password = db.Column(db.String(128), nullable=False)
    phone = db.Column(db.Integer, nullable=False)
    active_flag = db.Column(db.Integer, default=0)
    address = db.Column(db.String(128), default='NULL')


    def __init__(self, password, first_name, last_name, phone, active_flag, address, email):
        self.password = password
        self.first_name = first_name
        self.last_name = last_name
        self.email = emailvalidation(email)
        self.phone = phone
        self.active_flag = active_flag
        self.address = address
    def mailvailidation(self):
        return '{}.{}@email.com'.format(self.first_name, self.last_name)

class postschema(ma.Schema):
    class Meta:
        fields = ( "password", "first_name", "last_name", "email", "phone", "active_flag", "address")


psscheme = postschema()

posst = postschema(many=True)


@app.route('/register a user', methods = ['POST'])


def addpost():

    password = request.json['password']
    first_name = request.json['first_name']
    last_name = request.json['last_name']
    mail = request.json["email"]
    email = emailvalidation(mail)
    num = request.json["phone"]
    if isvalid(num):
        phone = request.json["phone"]
    else:
        phone = None

    active_flag = request.json["active_flag"]
    address = request.json["address"]
    hashed = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())
    table = Userservicedata(hashed, first_name, last_name, email, phone, active_flag, address)
    db.session.add(table)
    db.session.commit()


    return truncateuid()
@app.route( '/verify a user', methods = ['GET'] )
def verification():
    email = request.json['email']
    password = request.json['password']
    hashed = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())
    if not email:
        return "Missing Email"
    if not password:
        return "Password Missing"
    user = Userservicedata.query.filter_by(email=email).first()
    if not user:
        return "False"
    if bcrypt.checkpw(password.encode('utf-8'), hashed ):
        return "True"
    else:
        return "False"


@app.route('/', methods = ['GET'])
def get():
    allusers = Userservicedata.query.all()
    result = posst.dump(allusers)
    return jsonify(result)


@app.route('/Modify a user/<id>/', methods = ['PUT'])


def postdetails(id):
    poss = Userservicedata.query.get(id)
    password = request.json['password']
    first_name = request.json['first_name']
    last_name = request.json['last_name']
    email = request.json['email']
    phone = request.json['phone']
    active_flag = request.json['active_flag']
    address = request.json['address']

    hashed = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())

    poss.password = hashed
    poss.first_name = first_name
    poss.last_name = last_name
    poss.email = email
    poss.phone = phone
    poss.active_flag = active_flag
    poss.address = address
    db.session.commit()
    return "OK"

@app.route('/deactivate a user/<id>/', methods=['PATCH'])
def deactivate_the_user(id):
    deactvate_id = Userservicedata.query.get(id)
    active_flag = request.json['active_flag']
    if deactvate_id.active_flag == 1:
        deactvate_id.active_flag = active_flag
        db.session.commit()
        return "OK"
    else:
        return "the flag is aldready deactivated"

@app.route('/activate a user/<id>/', methods=['PATCH'])
def activate_the_user(id):
    actvate_id = Userservicedata.query.get(id)
    active_flag = request.json['active_flag']
    if actvate_id.active_flag == 0:
        actvate_id.active_flag = active_flag
        db.session.commit()
        return "OK"
    else:
        return "the flag is aldready in activation"
@app.route('/is user active/<id>/', methods=['PATCH'])
def is_user_active(id):
    check_user_active = Userservicedata.query.get(id)
    if check_user_active.active_flag == 1:
        return "True"
    else:
        return "False"




if __name__ == "__main__":
    app.run(debug=True)
