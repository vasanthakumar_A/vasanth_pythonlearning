import unittest
from package1.logintest import logintest
from package1.signuptest import signuptest
from package2.paymentreturntest import paymentreturntest
from package2.tc_paymenttest import paymenttest

tc1=unittest.TestLoader().loadTestsFromTestCase(logintest)
tc2=unittest.TestLoader().loadTestsFromTestCase(signuptest)
tc3=unittest.TestLoader().loadTestsFromTestCase(paymentreturntest)
tc4=unittest.TestLoader().loadTestsFromTestCase(paymenttest)

sanitytest = unittest.TestSuite([tc1, tc2])
funtionaltestsuite = unittest.TestSuite([tc3, tc4])
mastertestsuite = unittest.TestSuite([tc1, tc2, tc3, tc4])
unittest.TextTestRunner(verbosity=2).run(mastertestsuite)