from selenium import webdriver
from selenium.webdriver.common.by import By
import time

driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")

driver.get("https://seleniumhq.github.io/selenium/docs/api/java/index.html")
#driver.get("https://www.countries-ofthe-world.com/flags-of-the-world.html")

driver.find_element_by_xpath("/html/body/header/nav/div[1]/div[2]/ul[1]/li[1]/a").click()
driver.switch_to.frame("packageListFrame")
driver.find_element_by_link_text("org.openqa.selenium").click()
time.sleep(3)
driver.switch_to.default_content()
driver.switch_to.frame("packageFrame")
driver.find_element_by_link_text("WebDriver").click()
time.sleep(3)
driver.switch_to.default_content()
time.sleep(3)
driver.switch_to.frame("classFrame")
driver.find_element_by_xpath("/html/body/header/nav/div[1]/div[1]/ul/li[6]").click()