import logging
logging.basicConfig(filename="/home/decoders/test.log",                      #name of the file
                    format='%(asctime)s: %(levelname)s: %(message)s',        #in which format the file should be, here it should be in date, levelname, message
                    datefmt='%m/%d/%Y %I:&M:%s %p'                           #in which format the date should be
                    )
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)                                               #the debug should be mentioned otherwise the debug level and info level is not printed

logger.debug("this is a debug message")
logger.info("This is a info message")
logger.warning("This is a warning message")
logger.error("This is a error message")
logger.critical("This is a critical message")