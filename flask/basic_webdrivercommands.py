import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")

driver.get("http://demo.automationtesting.in/Windows.html")
print(driver.title)  #gets he title of the page
print(driver.current_url)  #gets the url of the page
driver.find_element_by_xpath("//*[@id='Tabbed']/a/button").click()
time.sleep(5)
#driver.close()     #closes currently focused browser
driver.quit()     #closes all browsers

