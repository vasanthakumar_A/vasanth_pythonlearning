from flask import  Flask, request, jsonify
from  flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow




app = Flask(__name__)


app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:root''@localhost/crud'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
ma = Marshmallow(app)

class post(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    title = db.Column(db.String(100))
    description = db.Column(db.String(200))
    author = db.Column(db.String(50))


    def __init__(self, title, description, author):
        self.title = title
        self.description = description
        self.author = author

class postschema(ma.Schema):
    class Meta:
        fields = ("title", "author", "description")

psscheme = postschema()

posst = postschema(many=True)

@app.route('/post', methods = ['POST'])
def addpost():
    title = request.json['title']
    description = request.json['description']
    author = request.json['author']

    mypost = post(title, description, author)
    db.session.add(mypost)
    db.session.commit()

    return psscheme.jsonify(mypost)



@app.route('/', methods = ['GET'])
def get():
    allposts = post.query.all()
    result = posst.dump(allposts)
    return jsonify(result)
@app.route('/<id>', methods = ['GET'])
def get_with_id(id):
    poss = post.query.get(id)

    return psscheme.jsonify(poss)

@app.route('/update/<id>/', methods = ['PUT'])


def postdetails(id):
  poss = post.query.get(id)

  title = request.json['title']
  description = request.json['description']
  author = request.json['author']

  poss.title = title
  poss.description = description
  poss.author = author

  db.session.commit()
  return psscheme.jsonify(poss)

@app.route('/delete/<id>/', methods = ['DELETE'])

def deletedetails(id):
    dele = post.query.get(id)
    db.session.delete(dele)
    db.session.commit()
    return psscheme.jsonify(dele)


if __name__ == "__main__":
    app.run(debug=True)