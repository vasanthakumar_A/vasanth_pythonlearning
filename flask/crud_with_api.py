from flask import Flask, render_template, url_for, request, redirect, json, jsonify
from flask_mysqldb import MySQL
from tabulate import tabulate
app = Flask(__name__)            #to get the name of the flask
app.config["MYSQL_HOST"]="localhost"
app.config["MYSQL_USER"]="root"
app.config["MYSQL_PASSWORD"]="root"
app.config["MYSQL_DB"]="crud"
app.config["MYSQL_CURSORCLASS"]="DictCursor"
mysql = MySQL(app)
@app.route('/')                  #condition for the page should open if there is no words after .com
@app.route('/home')
def home():

    con = mysql.connection.cursor()
    sql = "SELECT * FROM users"
    con.execute(sql)
    res = con.fetchall()
    users = []
    content = {}
    for result in res:
        content={'ID': result['ID'], 'NAME': result['NAME'], 'AGE': result['AGE'], 'CITY': result['CITY']}
        users.append(content)
        content={}
    return jsonify(users)
    #return render_template("home.html", datas=res)

@app.route('/addusers', methods=[ 'POST'])
def addusers():
    con = mysql.connection.cursor()
    json_dict = request.get_json()
    name = json_dict["NAME"]
    print("the name is", name)
    age = json_dict["AGE"]
    city = json_dict["CITY"]
    sql = "insert into users(name, age, city) values(%s, %s, %s)"
    user = (name, age, city)
    con.execute(sql, user)
    mysql.connection.commit()
    con.close()
    return "User Added"
    # if request.method == 'POST':
    #     n = request.form.get('name')
    #     a = request.form.get('age')
    #     c = request.form.get('city')
    #     con = mysql.connection.cursor()
    #     sql = "insert into users(NAME, AGE, CITY) values(%s, %s, %s)"
    #     user = (n,a,c)
    #     con.execute(sql, user)
    #     mysql.connection.commit()
    #     con.close()
    #     return jsonify()
    #     return redirect(url_for("home"))
    #return  render_template("addusers.html")
@app.route('/edituser/<string:id>', methods=['GET', 'POST', 'PUT'])
def edituser(id):


    con = mysql.connection.cursor()
    json_dict = request.get_json()
    name = json_dict["NAME"]
    print("the name is", name)
    age = json_dict["AGE"]
    city = json_dict["CITY"]
    sql = "update users set Name=%s, AGE = %s, CITY=%s where id=%s"
    user = (name, age, city, id)
    con.execute(sql, user)
    mysql.connection.commit()
    con.close()
    return "User updated"
    # if request.method == 'POST':
    #     n = request.form.get('name')
    #     a = request.form.get('age')
    #     c = request.form.get('city')
    #     con = mysql.connection.cursor()
    #     sql = "update users set Name=%s, AGE = %s, CITY=%s where id=%s"
    #     user = (n, a, c, id)
    #     con.execute(sql, user)
    #     mysql.connection.commit()
    #     con.close()
    #     return redirect(url_for("home"))
    # sql = "SELECT * FROM users where ID=%s"
    # con = mysql.connection.cursor()
    # con.execute(sql, [id])
    # res = con.fetchone()
    # return res
    #return render_template("edituser.html", datas=res)
@app.route('/deleteuser/<string:id>', methods=['GET', 'POST', 'DELETE'])
def deleteuser(id):

    # if request.method == 'POST':
    #     sql = "delete from users where id=%s"
    #     con.execute(sql, id)
    #     mysql.connection.commit()
    #     con.close()
    #     return redirect(url_for("home"))



    con = mysql.connection.cursor()
    sql = "delete from users where id=%s"
    con.execute(sql, [id])
    mysql.connection.commit()
    con.close()

    return "User Deleted"
    #res = con.fetchone()
    #return res
    # return render_template("delete.html", data=res)

if __name__ == "__main__":
    app.run(debug=True)