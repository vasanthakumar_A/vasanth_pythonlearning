import unittest
import requests
class apitest(unittest.TestCase):
    book_obj =  {
        "author": "789",
        "description": "this is a 789 description",
        "title": "this is a 789 title"
    }
    new_book_obj = {
        "author": "567",
        "description": "this is a 567 description",
        "title": "this is a 567 title"
    }

    def test_getdetails(self):
        r = requests.get("http://127.0.0.1:5000/")
        self.assertEqual(r.status_code, 200)
    @unittest.skip("addind data is skipped")
    def test_addnewdetails(self):
        r = requests.post("http://127.0.0.1:5000/post", json=apitest.book_obj)
        self.assertEqual(r.status_code, 200)
    @unittest.skip("skipped details added")
    def test_checkdetailsadded(self):
        r = requests.get("http://127.0.0.1:5000/3")
        self.assertEqual(r.status_code, 200)
        self.assertDictEqual(r.json(), apitest.book_obj)
    @unittest.skip("skipped updated")
    def test_updatedetails(self):

        r = requests.put("http://127.0.0.1:5000/update/14/", json=apitest.new_book_obj)
        self.assertEqual(r.status_code, 200)
    @unittest.skip("skip check updates")
    def test_check_for_updated_book(self):
        r = requests.get("http://127.0.0.1:5000/8")
        self.assertEqual(r.status_code, 200)
        self.assertDictEqual(r.json(), apitest.new_book_obj)
    @unittest.skip("skipped delete")
    def test_deletedetails(self):
        r = requests.delete("http://127.0.0.1:5000/delete/6/")
        self.assertEqual(r.status_code, 200)
    @unittest.skip("skip")
    def test_checkdetails_deleted(self):
        r = requests.get("http://127.0.0.1:5000/6")
        self.assertEqual(r.status_code, 200)





if __name__ == "__main__":
    unittest.main()